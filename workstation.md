# Windows Client TP 1
## I. Self-footprinting
### Host Os

Déterminer les principales informations de votre machine

**Nom de la machine**
```
PS C:\Users\berti> hostname                            ASUS-PILOU
```
**OS et version :**
```
PS C:\Users\berti> systeminfo                            Microsoft Windows 10
                                                         Famille // 10.0.18363 
                                                         N/A version 1836
```

**Architecture processeur :** 
```
PS C:\Users\berti> set                                   AMDx64
```

**Quantité RAM et modèle :**

* **RAM :** 
``` 
PS C:\Users\berti> Get-WMIObject win32_physicalmemory            Capacity:
                                                                 17179869184
```                                                              
* **Modèle :**
```
PS C:\Users\berti> Get-CimInstance win32_physicalmemory ||
Format-Table Manufacturer, PartNumber
                                                                 : Samsung 
                                                                  M471A2K43DB1-CTD
```

## **Devices**

Travail sur les périphériques branchés à la machine.

**Marque et modèle du processeur** : 

```
C:\Users\berti>wmic cpu get caption, deviceid, name, numberofcores, maxclockspeed, status


Caption                              DeviceID  MaxClockSpeed  Name                                           NumberOfCores  Status
AMD64 Family 23 Model 24 Stepping 1  CPU0      2300           AMD Ryzen 7 3750H with Radeon Vega Mobile Gfx  4              OK
```

**Marque et modèle du touchpad / Trackpad :**

```

```
**Marque et modèle de la carte graphique :**

```
C:\Users\berti>wmic path win32_VideoController get name         

Name
NVIDIA GeForce GTX 1650
AMD Radeon(TM) RX Vega 10 Graphics
```
**Disque dur :**

* **la marque et le modèle de votre(vos) disque(s) dur(s)**

```
C:\Users\berti>diskpart
```
"Open diskpart.exe"
```
DISKPART> list disk

  N° disque  Statut         Taille   Libre    Dyn  GPT
  ---------  -------------  -------  -------  ---  ---
  Disque 0    En ligne        476 G octets      0 octets        *

DISKPART> select disk 0

Le disque 0 est maintenant le disque sélectionné.

DISKPART> detail disk

Micron_2200V_MTFDHBA512TCK
ID du disque : {6E9995D9-AA1B-4B55-8B06-D356B3E1C545}
Type : NVMe
État : En ligne
Chemin : 0
Cible : 0
ID LUN : 0
Chemin d’accès de l’emplacement : PCIROOT(0)#PCI(0103)#PCI(0000)#NVME(P00T00L00)
État en lecture seule actuel : Non
Lecture seule : Non
Disque de démarrage : Oui
Disque de fichiers d’échange : Oui
Disque de fichiers de mise en veille prolongée : Non
Disque de fichiers de vidage sur incident : Oui
Disque en cluster  : Non

  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
  Volume 0     C   OS           NTFS   Partition    475 G   Sain       Démarrag
  Volume 1         SYSTEM       FAT32  Partition    260 M   Sain       Système
```
```
Name : Micron_2200V_MTFDHBA512TCK
```
* **les différentes partitions de votre/vos disque(s) dur(s)**
"open diskpart.exe"
```
DISKPART> list partition

  N° partition   Type              Taille   Décalage
  -------------  ----------------  -------  --------
  Partition 1    Système            260 M   1024 K
  Partition 2    Réservé             16 M    261 M
  Partition 3    Principale         475 G    277 M
  Partition 4    Récupération      1100 M    475 G
 
```
* **le système de fichier de chaque partition**
"Open diskpart.exe"
```
DISKPART> select partition 1

La partition 1 est maintenant la partition sélectionnée.

DISKPART> detail partition

Partition 1
Type    : c12a7328-f81f-11d2-ba4b-00a0c93ec93b
Masqué  : Oui
Requis  : Non
Attrib  : 0X8000000000000000
Décalage en octets : 1048576

  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
* Volume 1         SYSTEM       FAT32  Partition    260 M   Sain       Système
```
```
DISKPART> select partition 2

La partition 2 est maintenant la partition sélectionnée.

DISKPART> detail partition

Partition 2
Type    : e3c9e316-0b5c-4db8-817d-f92df00215ae
Masqué  : Oui
Requis  : Non
Attrib  : 0X8000000000000000
Décalage en octets : 273678336

Il n’y a pas de volume associé avec cette partition.
```
```
DISKPART> select partition 3

La partition 3 est maintenant la partition sélectionnée.

DISKPART> detail partition

Partition 3
Type    : ebd0a0a2-b9e5-4433-87c0-68b6b72699c7
Masqué  : Non
Requis  : Non
Attrib  : 0000000000000000
Décalage en octets : 290455552

  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
* Volume 0     C   OS           NTFS   Partition    475 G   Sain       Démarrag
```
```
DISKPART> select partition 4

La partition 4 est maintenant la partition sélectionnée.

DISKPART> detail partition

Partition 4
Type    : de94bba4-06d1-4d40-a16a-bfd50179d6ac
Masqué  : Non
Requis  : Oui
Attrib  : 0X8000000000000001
Décalage en octets : 510956404736

  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
* Volume 2         RECOVERY     NTFS   Partition   1100 M   Sain       Masqué
```
* **la fonction de chaque partition**
```
```
## **Users**

**liste des utilisateurs de la machine :**
```
PS C:\Users\berti> get-LocalUser

Name               Enabled Description
----               ------- -----------
Administrateur     False   Compte d’utilisateur d’administration
berti              True
DefaultAccount     False   Compte utilisateur géré par le système.
Invité             False   Compte d’utilisateur invité
WDAGUtilityAccount False   Compte d’utilisateur géré et utilisé par le système pour les scénarios Windows Defender A...
```
**Le nom d'utilisateur qui est full admin sur la machine :**
```
PS C:\Users\berti> $objSID = New-Object System.Security.Principal.SecurityIdentifier("S-1-5-20") 
$objAccount = $objSID.Translate([System.Security.Principal.NTAccount]) 
$objAccount.Value

AUTORITE NT\SERVICE RÉSEAU
```
## **Processus**

**Liste des processus de la machine :**

```
PS C:\Users\berti> ps

Handles  NPM(K)    PM(K)      WS(K)     CPU(s)     Id  SI ProcessName
-------  ------    -----      -----     ------     --  -- -----------
    266      16     4228      24368       0,63  14044   2 ApplicationFrameHost
   1288      42    30980      19252              3764   0 ArmouryCrate.Service
    903      56    33260      58104      28,92  10388   2 ArmouryCrate.UserSessionHelper
    217      19     3840       1000       0,78  18152   2 ArmourySocketServer
    282      20    20836       1540       0,56   9828   2 ArmourySwAgent
    253      75    24028      13372       5,95   1484   2 asus_framework
    421      60    26832       1680       0,69  10928   2 asus_framework
    291      13     3324       2692              3748   0 AsusLinkNear
     57       4      768       1192              3756   0 AsusLinkNearExt
    289      14     7672       9400              3820   0 AsusLinkRemote
    158       8     1792       3888              3084   0 AsusOptimization
    287      14     2708      14044       0,23   8204   2 AsusOptimizationStartupTask
    161      13     2340      12596       0,13   4224   2 AsusOSD
    453      19     9524      15612              3812   0 AsusSoftwareManager
    391      22    32204      33720       0,55   1052   2 AsusSoftwareManagerAgent
    601      12     3384       8408              3740   0 AsusSystemAnalysis
    154       8     1612        808              3780   0 AsusSystemDiagnosis
    265      12     2512      10604             16184   2 atieclxx
    174       8     1784       2200              2024   0 atiesrxx
    274      29     9976      26852       0,13   2836   2 backgroundTaskHost
    320      18     5416      23384       0,22  12632   2 backgroundTaskHost
    115       8     6524      11032       0,06   1964   2 conhost
    121       8     6512       1024       0,13   8644   2 conhost
    261      14     4528      15468       0,20  12236   2 conhost
    842      26     1964       5940               752   0 csrss
    581      17     4680       5420              5312   2 csrss
    400      15     3832      15064       6,34  18292   2 ctfmon
    216      13     3096      11904              4764   0 dasHost
    195      16     3260       3156              2056   0 dllhost
   1170      45   215384     141940              2824   2 dwm
   2529      87    70708     138240      52,16  18288   2 explorer
     38       5     1748        856              1056   0 fontdrvhost
     38       6     2640       6552             15768   2 fontdrvhost
    181      11     1736       1156             11596   0 GoogleCrashHandler
    160       9     1736        604             11612   0 GoogleCrashHandler64
    234      13     2672      17480       0,09  17020   2 GPU Power Saving
      0       0       60          8                 0   0 Idle
    159      10     1472       7516       0,02   1320   2 jusched
    313      21    11184       1928              4108   0 lghub_updater
    315      19     9660       9320              4116   0 LightingService
   1594      32     9140      15532                88   0 lsass
      0       0      580      27444              2516   0 Memory Compression
   1549      86   403616     327732              4456   0 MsMpEng
    192      12     6380      11132              9984   0 NisSrv
    668      63    12180      19628              4136   0 nvcontainer
    504      62    35820      54120      21,36  11260   2 nvcontainer
    342      18     5924      10980              1848   0 NVDisplay.Container
    685      31    32568      46420             10944   2 NVDisplay.Container
    620      77    30952       5392       8,66  16468   2 NVIDIA Web Helper
    721      28    37584      34252              3880   0 OfficeClickToRun
    562      28    41720       5892       1,17  14060   2 P508PowerAgent
    676      33    63096      74168       1,78  14304   2 powershell
      0      61     9496        148              9096   0 RefreshRateService
      0      16     9480      47340               120   0 Registry
    232      15     4932       3592              3772   0 remoting_host
    526      26    11656      14748       9,53   5844   0 remoting_host
    414      18     5872       7428              4408   0 ROGLiveService
    337      13     2740       4424              4316   0 RtkAudUService64
    333      12     2780       3116       0,84  17744   2 RtkAudUService64
   7023       9     1968       3452              4292   0 RtkBtManServ
    129       9     1956       9000       0,27   3996   2 rundll32
    342      18     5444      21148       0,34   4812   2 RuntimeBroker
    438      24     7980      33212       1,22  10248   2 RuntimeBroker
    237      14     3044      13580       0,16  11372   2 RuntimeBroker
    161       9     1836       8692       0,08  17012   2 RuntimeBroker
    735      77    37432      31560             11104   0 SearchIndexer
   1743     143   201996     262428      34,19  11804   2 SearchUI
    471      17     4692      16892              1084   0 SecurityHealthService
    152       9     1736       8768       0,13   1896   2 SecurityHealthSystray
    635      34    17116      20952              3688   0 servicehost
    812      11     5880      10144                96   0 services
    608      34     9636      21628       5,94  14144   2 SettingSyncHost
     89       7     3412       6124             16336   0 SgrmBroker
    822      35    40428      74240       3,61  10940   2 ShellExperienceHost
    561      19     7036      26820       7,11   2756   2 sihost
    410      23     7964      22392       0,23  14896   2 smartscreen
     53       3     1192       1112               472   0 smss
    434      21     5764       5104              3264   0 spoolsv
    765      35    52188      83428       5,58  13128   2 StartMenuExperienceHost
    456      18     7500      25680       1,55    924   2 svchost
     86       5      920        860              1032   0 svchost
   1257      25    14784      23068              1072   0 svchost
   1375      20     9772      13112              1180   0 svchost
    281      10     3012       5628              1232   0 svchost
    432      23     5864      15980              1388   0 svchost
    255      13     2584       5820              1392   0 svchost
    167      10     2312       4572              1432   0 svchost
    428      19     6968       9608              1464   0 svchost
    225      13     3252       7088              1504   0 svchost
    347      10     2280       5060              1520   0 svchost
    129       8     1484       2044              1576   0 svchost
    443      14    15696      13692              1672   0 svchost
    268      12     3080       6720              1792   0 svchost
    227      12     2444       5452              1920   0 svchost
    155      28     6452       6736              1932   0 svchost
    202      11     2132       2156              2016   0 svchost
    199      12     2640       5864              2036   0 svchost
    222      10     2320       3776              2044   0 svchost
    222      10     8648      14188              2072   0 svchost
    388      16     5180       8952              2148   0 svchost
    277      15     3556       4700              2192   0 svchost
    741      14     3088       5512              2264   0 svchost
    209       7     1344       1456              2328   0 svchost
    229      11     2652       5816              2336   0 svchost
    166       9     1900       3848              2356   0 svchost
    204      12     2328       3260              2472   0 svchost
    177      12     1960       4800              2564   0 svchost
    224      10     2160       5468              2596   0 svchost
    148       9     1704       2528              2604   0 svchost
    537      24     6692      10888              2648   0 svchost
    490      16     4936       9908              2736   0 svchost
    373      16     2748       5044              2896   0 svchost
    141      13     1848       2420              2900   0 svchost
    259      12     2956       6144              3032   0 svchost
    261      16     3088       7380              3104   0 svchost
    498      34    13388      16104              3364   0 svchost
    185      10     2056       2528              3392   0 svchost
    259      16     2896       4940              3564   0 svchost
    153       7     1552       2040              3668   0 svchost
    420      27     4668       9264              3796   0 svchost
    191       9     2152       3516              3832   0 svchost
    550      25    17568      22196              3840   0 svchost
    263      13     2676       2648              3944   0 svchost
    381      22    28196      29600              3952   0 svchost
    660      15     9468      13460              4032   0 svchost
    240      13     3120       5332              4040   0 svchost
    125       8     1540       3036              4128   0 svchost
    135       9     1580       1096              4180   0 svchost
    177      10     1968       3100              4392   0 svchost
    128       7     1368       2084              4420   0 svchost
    429      20     5316      14880              4436   0 svchost
    215      12     2368       1836              4496   0 svchost
    269      16     2796       4672              4552   0 svchost
    163       9     1784       8300       0,33   4560   2 svchost
    482      21     3640       7504              4572   0 svchost
    320      16     3732      22752       0,58   4652   2 svchost
    243      12     2900       7420              4660   0 svchost
    109       7     1268       1136              4948   0 svchost
    170       9     1748       6848              4960   0 svchost
    398      27     3536       5700              5236   0 svchost
    225      12     2824       2704              6436   0 svchost
    170      10     1796       8004              6704   0 svchost
    221      11     2140       4212              7008   0 svchost
    342      17     5220      16692              7120   0 svchost
    178       9     1784       3340              7448   0 svchost
    167       9     1668       6784              7504   0 svchost
    193      11     2036       6172              7564   0 svchost
    524      23     8596      36208       4,53   8216   2 svchost
    178      12     2464       4700              8780   0 svchost
    340      11     4592       6492              9004   0 svchost
    221      12     4840       9596              9160   0 svchost
    150       8     1776       2808              9816   0 svchost
    269      14     3520       5628             10344   0 svchost
    490      28     5764      24424       1,02  10564   2 svchost
    246      13     3072       6932             10792   0 svchost
    197      12     3820      11768             10844   0 svchost
    443      20     4548       9964             11188   0 svchost
    678      79    39864      42352             11368   0 svchost
    174     246     2344       7380             11444   0 svchost
    194      16     6848      10540             11896   0 svchost
    191      15     6484       2104             12360   0 svchost
    106       7     1392       2344             12912   0 svchost
    395      17     5880      16200             13016   0 svchost
    168      10     2044       3552             13596   0 svchost
    186      10     2188       6096             14184   0 svchost
    145      12     1576       6856             14648   0 svchost
    194      12     2632      10104             15016   0 svchost
    229      12     2624       9076             15412   0 svchost
    411      19     5328      17848             16880   0 svchost
    151       9     1768       7792             17084   0 svchost
    140       9     1588       1896             17348   0 svchost
    172       9     1944       2028             18044   0 svchost
   4537       0      212       3648                 4   0 System
    482      24     7184      31536       0,39   9464   2 SystemSettingsBroker
    784      39    26404      30960              6256   0 taskhostw
    310      32     7356      17704       0,88  11072   2 taskhostw
    490      29    14064      42552      10,84   8856   2 uihost
    126       7     1376       6664             10012   0 unsecapp
    555      24    33184      49944       1,67   3904   2 WindowsInternal.ComposableShell.Experiences.TextInput.Inpu...
    163      11     1532       6996               888   0 wininit
    274      12     2820      10296              5892   2 winlogon
    187      12     2684       6584              5916   0 WmiPrvSE
```
* **Csrss.exe**
Csrss signifie Client Server Run-time Subsystem.
C'est un sous-système essentiel qui doit fonctionner en permanence. Csrss gère les applications consoles, la création et la destruction de threads et quelques parties de l'environnement 16 bits virtuel MS-DOS.

* **System**
La plupart des threads du mode noyau fonctionnent en tant que processus System.

* **Winlogon.exe**
Il s'agit du processus responsable de gérer l'ouverture et la fermeture de session.
Winlongon est actif uniquement lorsque l'utilisateur appuie sur CTRL+ALT+DEL, à ce moment il affiche la boite de sécurité.

* **Services.exe**
Il s'agit du gestionnaire de contrôle des services (Service Control Manager).
Il est responsable du démarrage et de l'arrêt ainsi que de l'interaction avec les services système.

* **Spoolsv.exe**
Ce processus est responsable de la gestion des travaux d'impression et de fax...

**Processus lancés par l'utilisateur qui est full admin sur la machine :**

Ces processus sont ceux avec un **SI = 0**

## **Network**

**Liste des cartes réseau de la machine :**
```
PS C:\Windows\system32> Get-NetAdapter | fl name


name : Wi-Fi

name : Ethernet
```

**Wifi :**
La carte wifi est une norme de communication permettant la transmission de données numériques sans fil. Elle est appelée carte réseau compatible avec la norme WI-FI et équipée d’une antenne émettrice / réceptrice .Elle est également appelée Network Interface Card ( NIC ). Elle constitue l’Interface entre l’ordinateur et le câble du réseau. Sa fonction est de préparer , d’envoyer et de contrôler les données sur le réseau.

**Ethernet :**
Une carte réseau Ethernet sert d'interface physique entre l'ordinateur et le câble. Elle prépare pour le câble réseau les données émises par l'ordinateur, les transfère vers un autre ordinateur et contrôle le flux de données entre l'ordinateur et le câble. Elle traduit aussi les données venant du câble et les traduit en octets afin que l'Unité Centrale de l'ordinateur les comprenne. Ainsi une carte réseau est une carte d'extension s'insérant dans un connecteur d'extensions (slot).

**Tous les ports TCP et UDP en utilisation :**
```
PS C:\Windows\system32> netstat -a -b

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    0.0.0.0:135            Asus-Pilou:0           LISTENING
  RpcSs
 [svchost.exe]
  TCP    0.0.0.0:445            Asus-Pilou:0           LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:5040           Asus-Pilou:0           LISTENING
  CDPSvc
 [svchost.exe]
  TCP    0.0.0.0:5357           Asus-Pilou:0           LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49664          Asus-Pilou:0           LISTENING
 [lsass.exe]
  TCP    0.0.0.0:49665          Asus-Pilou:0           LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49666          Asus-Pilou:0           LISTENING
  EventLog
 [svchost.exe]
  TCP    0.0.0.0:49667          Asus-Pilou:0           LISTENING
  Schedule
 [svchost.exe]
  TCP    0.0.0.0:49668          Asus-Pilou:0           LISTENING
 [spoolsv.exe]
  TCP    0.0.0.0:49669          Asus-Pilou:0           LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:54902        Asus-Pilou:0           LISTENING
 [NVIDIA Web Helper.exe]
  TCP    127.0.0.1:55034        Asus-Pilou:65001       ESTABLISHED
 [nvcontainer.exe]
  TCP    127.0.0.1:65001        Asus-Pilou:0           LISTENING
 [nvcontainer.exe]
  TCP    127.0.0.1:65001        Asus-Pilou:55034       ESTABLISHED
 [nvcontainer.exe]
  TCP    192.168.0.12:139       Asus-Pilou:0           LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.0.12:55637     40.67.251.132:https    ESTABLISHED
  WpnService
 [svchost.exe]
  TCP    192.168.0.12:55927     bingforbusiness:https  ESTABLISHED
 [SearchUI.exe]
  TCP    192.168.0.12:55928     138.91.136.108:https   ESTABLISHED
 [SearchUI.exe]
  TCP    192.168.0.12:55931     13.107.9.254:https     ESTABLISHED
 [SearchUI.exe]
  TCP    192.168.0.12:55935     204.79.197.222:https   ESTABLISHED
 [SearchUI.exe]
  TCP    [::]:135               Asus-Pilou:0           LISTENING
  RpcSs
 [svchost.exe]
  TCP    [::]:445               Asus-Pilou:0           LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:5357              Asus-Pilou:0           LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:49664             Asus-Pilou:0           LISTENING
 [lsass.exe]
  TCP    [::]:49665             Asus-Pilou:0           LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:49666             Asus-Pilou:0           LISTENING
  EventLog
 [svchost.exe]
  TCP    [::]:49667             Asus-Pilou:0           LISTENING
  Schedule
 [svchost.exe]
  TCP    [::]:49668             Asus-Pilou:0           LISTENING
 [spoolsv.exe]
  TCP    [::]:49669             Asus-Pilou:0           LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [2a01:e0a:12c:2ee0:2dbb:acf5:65b:6aca]:55879  par21s04-in-x0a:https  ESTABLISHED
 [remoting_host.exe]
  TCP    [2a01:e0a:12c:2ee0:2dbb:acf5:65b:6aca]:55881  [2406:da14:88d:a100:2991:28c0:fdb2:df18]:https  TIME_WAIT
  TCP    [2a01:e0a:12c:2ee0:2dbb:acf5:65b:6aca]:55917  par21s03-in-x0e:https  TIME_WAIT
  TCP    [2a01:e0a:12c:2ee0:2dbb:acf5:65b:6aca]:55918  par21s17-in-x0a:https  TIME_WAIT
  TCP    [2a01:e0a:12c:2ee0:2dbb:acf5:65b:6aca]:55919  par10s28-in-x16:https  TIME_WAIT
  TCP    [2a01:e0a:12c:2ee0:2dbb:acf5:65b:6aca]:55920  par10s27-in-x01:https  TIME_WAIT
  TCP    [2a01:e0a:12c:2ee0:2dbb:acf5:65b:6aca]:55922  par10s29-in-x0e:https  TIME_WAIT
  TCP    [2a01:e0a:12c:2ee0:2dbb:acf5:65b:6aca]:55923  par10s27-in-x03:https  TIME_WAIT
  TCP    [2a01:e0a:12c:2ee0:2dbb:acf5:65b:6aca]:55925  [2620:1ec:c11::200]:https  ESTABLISHED
 [SearchUI.exe]
  TCP    [2a01:e0a:12c:2ee0:2dbb:acf5:65b:6aca]:55926  [2603:1026:c0a:855::2]:https  ESTABLISHED
 [SearchUI.exe]
  TCP    [2a01:e0a:12c:2ee0:2dbb:acf5:65b:6aca]:55929  [2600:1901:1:c36::]:https  ESTABLISHED
  WpnUserService_db48c72
 [svchost.exe]
  TCP    [2a01:e0a:12c:2ee0:2dbb:acf5:65b:6aca]:55930  par10s38-in-x0a:https  ESTABLISHED
 [remoting_host.exe]
  TCP    [2a01:e0a:12c:2ee0:2dbb:acf5:65b:6aca]:55932  [2603:1020:c01:2::3]:https  ESTABLISHED
 [SearchUI.exe]
  UDP    0.0.0.0:500            *:*
  IKEEXT
 [svchost.exe]
  UDP    0.0.0.0:3702           *:*
  FDResPub
 [svchost.exe]
  UDP    0.0.0.0:3702           *:*
  FDResPub
 [svchost.exe]
  UDP    0.0.0.0:4500           *:*
  IKEEXT
 [svchost.exe]
  UDP    0.0.0.0:5050           *:*
  CDPSvc
 [svchost.exe]
  UDP    0.0.0.0:5353           *:*
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:5355           *:*
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:50921          *:*
 [nvcontainer.exe]
  UDP    0.0.0.0:65138          *:*
  FDResPub
 [svchost.exe]
  UDP    127.0.0.1:1900         *:*
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:10020        *:*
 [NVIDIA Web Helper.exe]
  UDP    127.0.0.1:49664        *:*
  iphlpsvc
 [svchost.exe]
  UDP    127.0.0.1:52387        *:*
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:56913        *:*
 [nvcontainer.exe]
  UDP    192.168.0.12:137       *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.0.12:138       *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.0.12:1900      *:*
  SSDPSRV
 [svchost.exe]
  UDP    192.168.0.12:2177      *:*
  QWAVE
 [svchost.exe]
  UDP    192.168.0.12:5353      *:*
 [nvcontainer.exe]
  UDP    192.168.0.12:52386     *:*
  SSDPSRV
 [svchost.exe]
  UDP    [::]:500               *:*
  IKEEXT
 [svchost.exe]
  UDP    [::]:3702              *:*
  FDResPub
 [svchost.exe]
  UDP    [::]:3702              *:*
  FDResPub
 [svchost.exe]
  UDP    [::]:4500              *:*
  IKEEXT
 [svchost.exe]
  UDP    [::]:5353              *:*
  Dnscache
 [svchost.exe]
  UDP    [::]:5355              *:*
  Dnscache
 [svchost.exe]
  UDP    [::]:50922             *:*
 [nvcontainer.exe]
  UDP    [::]:65139             *:*
  FDResPub
 [svchost.exe]
  UDP    [::1]:1900             *:*
  SSDPSRV
 [svchost.exe]
  UDP    [::1]:5353             *:*
 [nvcontainer.exe]
  UDP    [::1]:52385            *:*
  SSDPSRV
 [svchost.exe]
  UDP    [2a01:e0a:12c:2ee0:29a6:817d:ac3a:9f8c]:2177  *:*
  QWAVE
 [svchost.exe]
  UDP    [2a01:e0a:12c:2ee0:2dbb:acf5:65b:6aca]:2177  *:*
  QWAVE
 [svchost.exe]
  UDP    [fe80::29a6:817d:ac3a:9f8c%14]:1900  *:*
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::29a6:817d:ac3a:9f8c%14]:2177  *:*
  QWAVE
 [svchost.exe]
  UDP    [fe80::29a6:817d:ac3a:9f8c%14]:52384  *:*
  SSDPSRV
 [svchost.exe]
```
**Fonction de chacun des programmes :**

**[svchost.exe] :** est un important processus Windows associé avec le service de systèmes Windows. Ce processus exécute des Bibliothèques de liens dynamiques.

**[lsass.exe] :** est le processus de Microsoft du Serveur local d'authentification de sécurité responsable pour l'authentification de l'identification de l'utilisateur et de l'application de la politique de sécurité.

**[spoolsv.exe] :** est un processus sécuritaire du système de Microsoft Windows, appelé "Spooler SubSystem App".

**[remoting_host.exe] :** est un composant logiciel de Chrome Remote Desktop de Google. C'est un fichier exécutable pour Windows.

**[NVIDIA Web Helper.exe] :** est un fichier signé Verisign. Certifié par un organisme de confiance. Ce n'est pas un composant système de Windows. Le processus écoute ou envoie des données sur les ports ouverts du LAN ou sur Internet.

**[nvcontainer.exe] :** Le processus utilise des ports pour se connecter à ou à partir d'un réseau local ou d'Internet. Nvcontainer.exe est capable de surveiller les applications.

**[SearchUI.exe] :** L'interface utilisateur de recherche est un processus faisant partie de l'assistant de recherche natif de Microsoft, Cortana. Il active l'interface utilisateur de recherche de l'assistant de recherche.

## II. Scripting
```
```

## III. Gestion des softs

**Expliquer l'intérêt de l'utilisation d'un gestionnaire de paquets :**

* Le gestionnaire de paquets est un système qui permet d'installer des logiciels, de les maintenir à jour et de les désinstaller. Son travail est de n'utiliser que des éléments compatibles entre eux, les installations sans utiliser de gestionnaire de paquets sont donc déconseillées.
* 

**Utiliser un gestionnaire de paquet propres à votre OS :**

*Liste des paquets déjà installés :*
```
PS C:\Windows\system32> choco list -l
Chocolatey v0.10.15
chocolatey 0.10.15
1 packages installed.
*
```
```
PS C:\Windows\system32> choco source
Chocolatey v0.10.15
chocolatey - https://chocolatey.org/api/v2/ | Priority 0|Bypass Proxy - False|Self-Service - False|Admin Only - False.
```

## IV. Machine Virtuelle
**3. Configuration post-install**
```
PS C:\Windows\system32> ssh root@192.168.120.51
root@192.168.120.51's password:
Last login: Mon Nov  9 17:07:54 2020
[root@localhost ~]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:2b:0d:c7 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global noprefixroute dynamic enp0s3
       valid_lft 86019sec preferred_lft 86019sec
    inet6 fe80::67f1:22ce:f03e:d821/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:35:f2:d9 brd ff:ff:ff:ff:ff:ff
    inet 192.168.120.51/24 brd 192.168.120.255 scope global noprefixroute dynamic enp0s8
       valid_lft 303sec preferred_lft 303sec
    inet6 fe80::4074:5b70:f1b7:7d2e/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```
**4.Partage de fichiers**

**installation de Samba :**
```
[root@localhost ~]# yum install -y cifs-utils
```
```
[root@localhost ~]# mkdir /opt/partage
[root@localhost ~]# mount -t cifs -o username=bertinpierrelouis@gmail.com,password=[...]//192.168.120.1/PartageVM /opt/partage
```
```
[root@localhost /]# cd opt/
[root@localhost opt]# cd partage/
```
```
[root@localhost partage]# touch test.txt
[root@localhost partage]# ls
test.txt
```
J'ai ensuite créer un fichier (test2) depuis mon windows dans le dossier PartageVM et je suis retourner dans la vm : 
```
[root@localhost partage]# ls
test2.txt  test.txt
```