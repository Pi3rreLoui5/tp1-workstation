﻿#script qui permet d'afficher certaine caractéristique de l'os
#Auteur : Pierre-Louis Bertin
#Date : 23/10/2020

Write-Output "Nom de l'ordinateur : $env:computername"
$Os = (Get-WMIObject win32_operatingsystem).name
$Os = $Os.split("|")[0]
Write-Output "Os : $Os"
$OSVersion = (Get-WMIObject win32_operatingsystem).version
Write-Output "Os Version : $OsVersion"
$Date = (Get-CimInstance Win32_OperatingSystem).LastBootUpTime 
Write-Output "Date et heure d'allumage : $Date"
$criteria = "Type='software' and IsAssigned=1 and IsHidden=0 and IsInstalled=0"
$searcher = (New-Object -COM Microsoft.Update.Session).CreateUpdateSearcher()
$updates = $searcher.Search($criteria).Updates
if ($updates.Count -ne 0) {
    $osUpdated = "Non"
}
else {
    $osUpdated = "Oui"
}
Write-Output "est-ce que l'os est a jour : $osUpdated"

Write-Output " "

$RamTotale=[STRING]((Get-WmiObject -Class Win32_ComputerSystem ).TotalPhysicalMemory/1GB)
Write-Output "Ram totale : $RamTotale"
$RamLibre = (Get-CIMInstance Win32_OperatingSystem).FreePhysicalMemory
Write-Output "RAM disponible : $RamLibre Go"
$RamUtiliser=[String]((Get-WmiObject -Class Win32_OperatingSystem).FreePhysicalMemory/1MB)
Write-Output "Ram utiliser : $RamUtiliser Go"
$DiskDispo = [Math]::Round((Get-Volume -DriveLetter 'C').Size/1GB)
Write-Output "Espace disque disponible : $DiskDispo Go"
$DiskUse = [Math]::Round((Get-Volume -DriveLetter 'C').SizeRemaining/1GB)
Write-Output "Espace disque utiliser : $DiskUse Go"

Write-Output " "

$ip = (Test-Connection -ComputerName $env:computername -count 1).IPV4Address.ipaddressTOstring
Write-Output "Ip principale : $ip"
$connection = (Test-Connection -ComputerName "8.8.8.8" -Count 4  | measure-Object -Property ResponseTime -Average).average
Write-Output " - Ping : $connection ms"
$DownloadSpeed = [math]::Round($SpeedtestResults.download.bandwidth / 1000000 * 8, 2)
$UploadSpeed = [math]::Round($SpeedtestResults.upload.bandwidth / 1000000 * 8, 2)
Write-Output " - download speed : $DownloadSpeed Mbit/s"
Write-Output " - upload speed : $UploadSpeed Mbit/s"

Write-Output " "

$Users = (Get-WmiObject Win32_UserAccount).Name 
Write-Output "La liste des utilisateurs : $Users"
